<?php
namespace Jad\Blog\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Registry;

class Index extends \Magento\Framework\App\Action\Action
{

    protected $_registry;

    const URLKEY = "/blog/index/index/";

    public function __construct(
        Context $context,
        Registry $registry
    ){
        $this->_registry=$registry;
        parent::__construct($context);
    }

    public function execute()
    {
        $id=$this->getRequest()->getParam('id')??2;
        $this->_registry->register('entity_id',$id);
        $this->_registry->register('urlkey',self::URLKEY);


        return $this->resultFactory->create(ResultFactory::TYPE_PAGE);
    }

}
