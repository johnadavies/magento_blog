<?php

//https://devdocs.magento.com/guides/v2.3/extension-dev-guide/routing.html

declare(strict_types=1);

namespace Jad\Blog\Controller;

use Jad\Blog\Api\Repositories\EntityRepositoryInterface;
use Magento\Framework\App\Action\Forward;
use Magento\Framework\App\ActionFactory;
use Magento\Framework\App\ActionInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\RouterInterface;

/**
 * Class Router
 */
class Router implements RouterInterface
{
    /**
     * @var ActionFactory
     */
    private $actionFactory;

    /**
     * @var ResponseInterface
     */
    private $response;

    /**
     * Router constructor.
     *
     * @param ActionFactory $actionFactory
     * @param ResponseInterface $response
     */
    public function __construct(
        ActionFactory $actionFactory,
        ResponseInterface $response,
        EntityRepositoryInterface $entityInterface
    ) {
        $this->actionFactory = $actionFactory;
        $this->response = $response;
        $this->entityInterface=$entityInterface;
    }

    /**
     * @param RequestInterface $request
     * @return ActionInterface|null
     */
    public function match(RequestInterface $request): ?ActionInterface
    {
        $identifier = trim($request->getPathInfo(), '/');
        if (strpos($identifier, 'blogger') !== false) {
            $identifier = trim($identifier, '/blogger');

            $entity=$this->entityInterface->findByUrlKey($identifier);

            $request->setModuleName('blog'); //defined in routes.xml "Id"
            $request->setControllerName('index'); //Folder in Controller
            $request->setActionName('index'); //controller class
            $request->setParams([
                'id' => $entity->getId()
            ]);

            return $this->actionFactory->create(Forward::class, ['request' => $request]);
        }

        return null;
    }
}
