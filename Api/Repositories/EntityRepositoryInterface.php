<?php
namespace Jad\Blog\Api\Repositories;

use Jad\Blog\Api\Data\EntityInterface;

interface EntityRepositoryInterface
{

    public function browse();

    public function read(int $id);

    public function edit(int $id,EntityInterface $model);

    public function add(EntityInterface $model);

    public function delete(EntityInterface $model);

}
