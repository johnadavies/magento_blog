<?php
namespace Jad\Blog\Model\ResourceModel\Entity;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{

    protected $_idFieldName='entity_id';

    protected function _construct()
    {
        $this->_init(
            \Jad\Blog\Model\Entity::class,
            \Jad\Blog\Model\ResourceModel\Entity::class
        );


    }

    public function filterByChildren($entity_id)
    {
        $relationsTable=$this->getResource()->getTable('jad_blog_entity_relations');
        $this->getSelect()->joinLeft(
            $relationsTable,
            'main_table.entity_id=' .$relationsTable. '.child_id '
        )->where($relationsTable.'.parent_id = ?',$entity_id);;


        return $this;
    }



}
