<?php
namespace Jad\Blog\Model\Repositories;

use Jad\Blog\Model\ResourceModel\Entity\CollectionFactory;
use Jad\Blog\Api\Data\EntityInterface;
use Jad\Blog\Model\Entity;

class EntityRepository implements \Jad\Blog\Api\Repositories\EntityRepositoryInterface
{

    private $entityCollection;
    private $entityInterface;
    private $entity;

    public function __construct(
        CollectionFactory $entityCollection,
        EntityInterface $entityInterface,
        Entity $entity
    ){
        $this->entityCollection=$entityCollection;
        $this->entityInterface=$entityInterface;
        $this->entity = $entity;

    }

    /** BREAD OPERATIONS */

    public function browse()
    {
        return $this->entityCollection->create();
    }

    /**
     * @param int $id
     * @return EntityInterface
     */
    public function read(int $id)
    {
        return $this->entityInterface->load($id);
    }

    public function edit(int $id,EntityInterface $model)
    {

    }

    public function add(EntityInterface $model)
    {
        return $this->entityInterface->save($model);
    }

    public function delete(EntityInterface $model)
    {

    }

    /** HELPER OPERATIONS */

    public function findByUrlKey($url="")
    {
        return $this->entityInterface->load($url,'url_key');
    }



}
