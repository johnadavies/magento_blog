<?php

namespace Jad\Blog\Model;
use Jad\Blog\Api\Data\EntityInterface;
use Magento\Framework\Model\AbstractModel;

class Entity extends AbstractModel implements EntityInterface
{

    protected function _construct()
    {
        $this->_init(\Jad\Blog\Model\ResourceModel\Entity::class);
    }



}

